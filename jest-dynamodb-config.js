module.exports = {
  tables: [
    {
      TableName: 'superhero-table',
      KeySchema: [{ AttributeName: 'Id', KeyType: 'HASH' }],
      AttributeDefinitions: [{ AttributeName: 'Id', AttributeType: 'N' }],
      ProvisionedThroughput: { ReadCapacityUnits: 1, WriteCapacityUnits: 1 },
    },
  ],
  port: 8000,
};
// partitionKey: { name: 'Id', type: dynamodb.AttributeType.NUMBER },
// readCapacity: 5,
// writeCapacity: 5,
