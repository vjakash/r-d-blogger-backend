RightStart NodeJS CDK

# Deployment, Tooling and Configuration

Stack Configuration `/infra/constants`

- Configurations like StackName, Stack Prefix and additional details needed for the stack can be provided here
- Additional configurations like pipeline and cognito configurations are also provided here.


## Deploying via CDK

! IMPORTANT

Always make sure that the CDK version and the dependencies version are same, any minor version change will raise compatibility issues.If you are using external dependencies , make sure that it is compatible with the cdk version you are using. For Eg:

"@aws-cdk/aws-apigateway": "1.92.0",
"@aws-cdk/aws-autoscaling": "1.92.0",
"@aws-cdk/aws-codebuild": "1.92.0",
"@aws-cdk/aws-dynamodb": "1.92.0",

There might not be a high level cdk construct available for all the cloudformation resources, some of them have a workaround by switching to L2 constructs , which are low level constructs. 

For Eg: there are no high level constructs available for api gateway with websocket protocol.

Some constructs might be unstable, refer the cdk guide for breaking changes.

# Commands

`cdk synth` - This will generate a cloudformation yaml output based on the code.

`cdk deploy` - This will deploy the code, make sure the right env is set in cli.
There are other flags, for eg: `--require-approval never` which will not ask for confirmation ,before deploying, the outputs can be written to a JSON file with `-O ./cdk.out/deploy-output.json` which can then be used for configuration generation or can be passed to frontend repo.

`cdk bootstrap` - Bootstrapping of aws account is necessary if the stack contains assets like files or cloudformation template size limit is reached.

# Testing

- Jest
- Jest-Cucumber
- Sinon
- Shelf-DynamoDB Preset
- CDK Assert

Jest is configured with Shelf-DynamoDB preset, which allows to mock dynamodb calls, it also requires docker image of local dynamo db to be running.
The configuration such as table details and port for dynamo-db is provided in `jest-dynamodb-config`

Since the tests are written in typescript, a transform function is used `transform: { '^.+\\.tsx?$': 'ts-jest' },`

Sinon is used to create spy and stub functions.

Jest-Cucumber allows you to load Gherkin features and write test cases for them.
The features can be defined in a .feature file and gherkin features can be generated using the plugin Jest-cucumber code generator.
Plugin Link:
https://marketplace.visualstudio.com/items?itemName=Piotr-Porzuczek.jest-cucumber-code-generator-extension

For Unit test, jest is used.
For CDK Tests, Jest along with AWS-CDK assert is used.


# Linting and Hooks
GUI apps on macOS don’t have /usr/local/bin in their PATH by default. To fix this for Git Clients like sourcetree,
Create a file .huskyrc on the user root `~/.huskyrc`
with
```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```

- Precommit hooks are configured using husky. ES Lint rules by AirBnB and Prettier is ran before the commit is made.
- Same rules are applied in codebuild phase to check for issues.

# Deploying the SuperHero Project

Super hero API returns a random superhero from the set of 10 , everytime when the api is called.

## Setup
- Do `npm i` on root folder and also on the infra folder
- Navigate to `infra` folder, and deploy the code using `cdk-deploy`
- Make a note of dynamo-db table name created, and copy it to the data.json file inside the data folder
- Run the command `aws dynamodb batch-write-item --request-items file://data.json --return-consumed-capacity TOTAL` to do a bulk insert.
- The API can then be called to get a random superhero.

Structure (Infra)
```
.
├── README.md
├── bin
│   └── infra.ts // Configurations needed to create stack
├── buildspec.yaml // Buildspec for backend deployment
├── cdk.json
├── data
│   ├── README.md
│   └── data.json // Mock Data
├── jest.config.js // Jest Configurations
├── lib
│   ├── constants // All project related constants
│   ├── functions // Modularized code for lambda functions and related assets
│   ├── infra-stack.ts // Entry point to CDK which initializes the code
│   ├── resources // Contains pipelines,rest api gateway and static site resources
│   └── utils // Project related utilities
├── package-lock.json
├── package.json
├── test
│   └── infra.test.ts // Test cases for Infra
└── tsconfig.json
```

Backend 
```
├── api
│   └── get-superhero.ts (API Entrypoints, Lambda Handlers)
├── helpers
│   └── index.ts 
├── models
│   └── superhero.model.ts 
├── services
│   └── superhero.service.ts 
└── utils 
    ├── logging.ts
    └── response.ts
```