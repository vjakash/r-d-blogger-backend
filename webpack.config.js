// const path = require('path');
// const entryPlus = require('webpack-entry-plus');
// // const slsw = require('serverless-webpack');
// const nodeExternals = require('webpack-node-externals');
// const CopyWebpackPlugin = require('copy-webpack-plugin');
// const glob = require('glob');

// const entryPoints = [
//   {
//     entryFiles: glob.sync('./src/api/*.ts'),
//     outputName: (item) => {
//       const fileName = item.split('/');
//       const fileNameWithoutFmt = fileName[fileName.length - 1].split('.')[0];
//       return fileNameWithoutFmt;
//     },
//   },
// ];

// module.exports = {
//   // mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
//   entry: entryPlus(entryPoints),
//   resolve: {
//     extensions: ['.ts', 'tsx'],
//   },
//   externals: [nodeExternals()], // this is required
//   plugins: [
//     new CopyWebpackPlugin({ patterns: ['./prisma/schema.prisma'] }), // without this the prisma generate above will not work
//   ],
//   target: 'node',
//   module: {
//     rules: [
//       {
//         test: /\.(tsx?)$/,
//         loader: 'ts-loader',
//         exclude: [
//           [
//             path.resolve(__dirname, 'node_modules'),
//             path.resolve(__dirname, '.serverless'),
//             path.resolve(__dirname, '.webpack'),
//           ],
//         ],
//       },
//     ],
//   },
// };

const path = require('path');
const entryPlus = require('webpack-entry-plus');
const nodeExternals = require('webpack-node-externals');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const glob = require('glob');

const entryPoints = [
  {
    entryFiles: glob.sync('./src/api/*.ts'),
    outputName: (item) => {
      const fileName = item.split('/');
      const fileNameWithoutFmt = fileName[fileName.length - 1].split('.')[0];
      return fileNameWithoutFmt;
    },
  },
];

module.exports = {
  context: __dirname,
  entry: entryPlus(entryPoints),
  externals: [nodeExternals({
    allowlist:['@prisma/client']
  })], // this is required
  plugins: [
    new CopyWebpackPlugin({ patterns: ['./prisma/schema.prisma'] }), // without this the prisma generate above will not work
  ],
  resolve: {
    extensions: ['.js', '.json', '.ts'],
    symlinks: false,
    cacheWithContext: false,
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, '.webpack'),
    filename: '[name]/handler.js',
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        loader: 'ts-loader',
        exclude: [
          [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, '.serverless'),
            path.resolve(__dirname, '.webpack'),
          ],
        ],
      },
    ],
  },
  optimization: {
    minimize: true,
  }
};
