Replace the table name in data.json after deployment and bulk insert into dynamo db using the command below
`aws dynamodb batch-write-item --request-items file://data.json --return-consumed-capacity TOTAL`
