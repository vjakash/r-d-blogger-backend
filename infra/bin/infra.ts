#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import InfraStack from '../lib/infra-stack';
import InfraStackProps from '../lib/constants';

const app = new cdk.App();
new InfraStack(app, 'blogger-app-stack', InfraStackProps);
