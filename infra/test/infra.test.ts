import {
  expect as expectCDK,
  matchTemplate,
  MatchStyle,
  countResources,
  haveResourceLike,
  haveResource,
} from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import InfraStackProps from '../lib/constants';
import InfraStack from '../lib/infra-stack';

test('Empty Stack', () => {
  const app = new cdk.App();
  // WHEN
  const stack = new InfraStack(app, 'MyTestStack', InfraStackProps);
  // THEN
  expectCDK(stack).to(countResources('AWS::DynamoDB::Table',1));
  expectCDK(stack).to(countResources('AWS::CloudFront::Distribution',1));
});
