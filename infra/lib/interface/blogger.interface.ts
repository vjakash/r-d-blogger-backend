import { LayerVersion } from '@aws-cdk/aws-lambda';
import InfraStackProps from "../constants";
import { CognitoUserPool } from '../resources/cognito/cognito';
import { BloggerAppVPC } from '../resources/network/vpc';

import { RdsMysqlDB } from '../resources/table/rds-mysql';


export interface IPrismaLayerProps{
  props:typeof InfraStackProps,
}

export interface ILambdas{
    props:typeof InfraStackProps,
    bloggerAppVPC:BloggerAppVPC,
    cognitoUserPool:CognitoUserPool,
    rdsInstance:RdsMysqlDB,
    prismaLayer:LayerVersion
}

export interface IUserPool{
  props:typeof InfraStackProps,
  cognitoUserPool:CognitoUserPool,
}


