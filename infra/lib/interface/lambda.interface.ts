import { LayerVersion } from '@aws-cdk/aws-lambda';
import InfraStackProps from "../constants";
import { BloggerAppVPC } from '../resources/network/vpc';

import { RdsMysqlDB } from '../resources/table/rds-mysql';

export interface ILambdaMethod{
    id: string;
    name: string;
    method?: string;
    lambda: {
      code: string;
      handler: string;
      timeout?: number;
    };
    http?: {
      endpoint: string;
    };
    auth?: boolean;
}
export interface ILambdasConstant{
    resourceName:string;
    childResource?:ILambdasConstant[];
    methods?:ILambdaMethod[]
}
  
export interface ITriggersProps{
    props:typeof InfraStackProps,
    bloggerAppVPC:BloggerAppVPC,
    rdsInstance:RdsMysqlDB,
    prismaLayer:LayerVersion
}
export interface ITriggers{
    postConfirmation:ILambdaMethod;
}
