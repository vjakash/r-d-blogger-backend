import { Triggers } from '../resources/lambdas/triggers';
import InfraStackProps from "../constants";


export interface ICognitoUserpoolProps{
    props:typeof InfraStackProps,
    triggers:Triggers
  }