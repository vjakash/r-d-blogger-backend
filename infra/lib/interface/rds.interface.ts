import InfraStackProps from "../constants";
import { BloggerAppVPC } from '../resources/network/vpc';


export interface IRdsProps{
    props:typeof InfraStackProps,
    bloggerAppVPC:BloggerAppVPC,
}