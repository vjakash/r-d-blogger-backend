import { Construct, RemovalPolicy, Tags } from '@aws-cdk/core';
import { StaticWebsite } from '@cloudcomponents/cdk-static-website';
import InfraStackProps from '../constants';

class StaticWebsiteStack extends Construct {
  public readonly staticWebsite: StaticWebsite;

  constructor(scope: Construct, id: string, props?: typeof InfraStackProps) {
    super(scope, id);
    this.staticWebsite = new StaticWebsite(this, 'static-website', {
      bucketConfiguration: {
        removalPolicy: RemovalPolicy.DESTROY,
      },
    });

    Tags.of(this).add('Stack', props?.stackPrefix ? props.stackPrefix : '');
  }
}

export default StaticWebsiteStack;
