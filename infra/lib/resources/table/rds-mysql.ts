import { Construct } from "@aws-cdk/core";
import * as rds from '@aws-cdk/aws-rds'
import * as ec2 from '@aws-cdk/aws-ec2'
import * as cdk from '@aws-cdk/core';
import { IRdsProps } from "../../interface/rds.interface";


export class RdsMysqlDB extends Construct{
    readonly rds:rds.DatabaseInstance;

    constructor(scope: Construct, id: string, props: IRdsProps){
        super(scope,id);
        const publicSg = new ec2.SecurityGroup(this, `${props.props.stackPrefix}-private-sg`, {
            vpc:props.bloggerAppVPC.vpc,
            securityGroupName: `${props.props.stackPrefix}-private-sg`,
          })
        publicSg.addIngressRule(ec2.Peer.anyIpv4(),ec2.Port.tcp(3306),'Opening RDS to Lambda')

        this.rds=new rds.DatabaseInstance(this,`${props.props.stackPrefix}-db`,{
            engine: rds.DatabaseInstanceEngine.mysql({
              version: rds.MysqlEngineVersion.VER_8_0_15
            }),
            vpc:props.bloggerAppVPC.vpc,
            instanceType: ec2.InstanceType.of(
              ec2.InstanceClass.T2,
              ec2.InstanceSize.MICRO
            ),
            allocatedStorage: 20,
            securityGroups: [publicSg],
            databaseName: 'blogger',
            publiclyAccessible: false,
            removalPolicy: cdk.RemovalPolicy.DESTROY,
            autoMinorVersionUpgrade: false,
        })
    }
}