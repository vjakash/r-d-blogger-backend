
import { Construct } from "@aws-cdk/core";
import * as ec2 from '@aws-cdk/aws-ec2';

import { IRdsProps } from "../../interface/rds.interface";



export class BastionHost extends Construct{
    readonly bastionHost:ec2.BastionHostLinux;

    constructor(scope: Construct, id: string, props: IRdsProps){
        super(scope,id);
        this.bastionHost=new ec2.BastionHostLinux(this,`${props?.props?.stackPrefix}-bastionhost`,{
            vpc:props.bloggerAppVPC.vpc,
            subnetSelection:{subnetType:ec2.SubnetType.PUBLIC},
        });
        this.bastionHost.instance.instance.addPropertyOverride('KeyName','bastion-key')
    }
}