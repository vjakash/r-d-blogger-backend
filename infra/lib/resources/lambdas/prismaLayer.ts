
import { Code, LayerVersion, Runtime } from "@aws-cdk/aws-lambda";
import { Construct } from "@aws-cdk/core";
import {  IPrismaLayerProps } from "../../interface/blogger.interface";


export class PrismaLayer extends Construct{
    readonly prismaLayer:LayerVersion;
    
    constructor(scope: Construct, id: string, props: IPrismaLayerProps){
        super(scope,id);

        this.prismaLayer= new LayerVersion(this,`${props.props.stackPrefix}-prismaLayer`,{
            code:Code.fromAsset('/Users/vj/RD-task/r-d-blogger-backend/src/layers/prismaClient/prismaClient.zip'),
            compatibleRuntimes:[Runtime.NODEJS_10_X],
            description:'Layer for prisma client',
            layerVersionName:`${props.props.stackPrefix}-prismaLayer`
        })
    }

    
}