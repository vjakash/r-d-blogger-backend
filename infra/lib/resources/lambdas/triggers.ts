
import { AssetCode, Function, IFunction, Runtime } from "@aws-cdk/aws-lambda";
import { Construct, Duration } from "@aws-cdk/core";
import { triggers } from "../../constants/triggers.constants";
import {  ITriggersProps } from "../../interface/lambda.interface";


export class Triggers extends Construct{
   readonly postConfirmation:IFunction
    
    constructor(scope: Construct, id: string, props: ITriggersProps){
        super(scope,id);
            this.postConfirmation=new Function(this, `${props.props.stackPrefix}-${triggers.postConfirmation.id}-Function`, {
                code: new AssetCode(`/Users/vj/RD-task/r-d-blogger-backend/.webpack/${triggers.postConfirmation.lambda.code}`),
                handler: `handler.${triggers.postConfirmation.lambda.handler}`,
                runtime: Runtime.NODEJS_10_X,
                functionName: `${props.props.stackPrefix}-${triggers.postConfirmation.name}-function`,
                vpc: props.bloggerAppVPC.vpc,
                timeout: Duration.seconds(30),
                environment: {
                    DATABASE_URL:`mysql://${props.rdsInstance.rds.secret?.secretValueFromJson('username')}:${props.rdsInstance.rds.secret?.secretValueFromJson('password')}@${props.rdsInstance.rds.dbInstanceEndpointAddress}:${props.rdsInstance.rds.dbInstanceEndpointPort}/blogger?connection_limit=1`
                },
                layers:[props.prismaLayer]
            });
            props.rdsInstance.rds.grantConnect(this.postConfirmation);
    }

    
}