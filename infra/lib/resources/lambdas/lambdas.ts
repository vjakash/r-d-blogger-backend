/* eslint-disable no-restricted-syntax */
import { EndpointType, RestApi, AuthorizationType, Resource, CfnAuthorizer, LambdaIntegration, IResource, Cors} from "@aws-cdk/aws-apigateway";
import { AssetCode, Function, IFunction, Runtime } from "@aws-cdk/aws-lambda";
import { Construct, Duration } from "@aws-cdk/core";
import {lambdas} from "../../constants/lambdas.constant";
import { ILambdaMethod, ILambdasConstant } from "../../interface/lambda.interface";
import {  ILambdas} from "../../interface/blogger.interface";


export class Lambdas extends Construct{

    readonly auth:CfnAuthorizer;
    
    constructor(scope: Construct, id: string, props: ILambdas){
        super(scope,id);

       
        const restApi=new RestApi(this,`${props.props.stackPrefix}-apiGateway`,{
            endpointTypes:[EndpointType.REGIONAL],
            defaultCorsPreflightOptions:{
                allowOrigins:Cors.ALL_ORIGINS,
                allowHeaders:Cors.DEFAULT_HEADERS,
                allowMethods:Cors.ALL_METHODS
            },
            
        })
        this.auth = new CfnAuthorizer(this, `${props.props.stackPrefix}-APIGatewayAuthorizer`,{
            name: `${props.props.stackPrefix}-cognito-authorizer`,
            identitySource: 'method.request.header.Authorization',
            providerArns: [props.cognitoUserPool.userPool.userPoolArn],
            restApiId: restApi.restApiId,
            type: AuthorizationType.COGNITO,
        });
        this.createResource(lambdas,props,restApi.root);
    }

    createResource(resources:ILambdasConstant[],props:ILambdas,rootResource: IResource){
        for(const resource of resources){
            const resourcePart:Resource=rootResource.addResource(resource.resourceName);
            
            if (resource.childResource && resource.childResource.length > 0) {
                this.createResource(resource.childResource, props, resourcePart);
            }
            if (resource.methods && resource.methods.length > 0) {
                resource.methods.forEach((method) => {
                  if (method.lambda) {
                    const lambda = this.createFunction(method, props);
                    this.createApigwIntegration(method, resourcePart, lambda);
                  }
                });
            }
        }
    }

    createFunction(method:ILambdaMethod,props:ILambdas){
        const handler=new Function(this, `${method.id}-Function`, {
            code: new AssetCode(`/Users/vj/RD-task/r-d-blogger-backend/.webpack/${method.lambda.code}`),
            handler: `handler.${method.lambda.handler}`,
            runtime: Runtime.NODEJS_10_X,
            functionName: `${method.name}-function`,
            vpc: props.bloggerAppVPC.vpc,
            timeout: Duration.seconds(30),
            environment: {
                DATABASE_URL:`mysql://${props.rdsInstance.rds.secret?.secretValueFromJson('username')}:${props.rdsInstance.rds.secret?.secretValueFromJson('password')}@${props.rdsInstance.rds.dbInstanceEndpointAddress}:${props.rdsInstance.rds.dbInstanceEndpointPort}/blogger?connection_limit=1`
            },
            layers:[props.prismaLayer]
        });
        props.rdsInstance.rds.grantConnect(handler);
        return handler;
    }

    createApigwIntegration(
        lambdaParams: ILambdaMethod,
        resourcePart: Resource,
        lambda: IFunction
    ){            
        const newIntegration = new LambdaIntegration(lambda);
        if(lambdaParams.auth){
            resourcePart.addMethod(lambdaParams.method || '', newIntegration, { 
                authorizer: {
                    authorizationType: AuthorizationType.COGNITO,
                    authorizerId: this.auth.ref
                },
            });
        }else{
            resourcePart.addMethod(lambdaParams.method || '', newIntegration);
        }
        resourcePart.node.addDependency(this.auth);
    }
}