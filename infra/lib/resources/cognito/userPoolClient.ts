
import { Construct } from "@aws-cdk/core";
import { UserPoolClient,UserPoolIdentityProviderFacebook ,ProviderAttribute,UserPoolIdentityProviderGoogle,OAuthScope,UserPoolClientIdentityProvider,UserPoolDomain} from '@aws-cdk/aws-cognito'
import { IUserPool } from "../../interface/blogger.interface";


export class CognitoUserPoolClient extends Construct{
    private readonly userPoolClient:UserPoolClient;

    constructor(scope: Construct, id: string, props: IUserPool){
        super(scope,id);
        new UserPoolIdentityProviderFacebook(this,`${props.props.stackPrefix}-userpool-identityProvider-facebook`,{
              clientId:'195100416054208',
              clientSecret:'db6660e5bb089cefbaf9b747c6e572c3',
              userPool:props.cognitoUserPool.userPool,
              attributeMapping:{
                email:ProviderAttribute.FACEBOOK_EMAIL,
                fullname:ProviderAttribute.FACEBOOK_FIRST_NAME
              },
              scopes:["profile","email" ,"openid"]
          });
          new UserPoolIdentityProviderGoogle(this,`${props.props.stackPrefix}-userpool-identityProvider-google`,{
              clientId:'136888953363-ikle83u0jn7injj6v6inuv35ka0kdu6u.apps.googleusercontent.com',
              clientSecret:'GmzmVhmViUz3OR651Ks8NG-W',
              userPool:props.cognitoUserPool.userPool,
              attributeMapping:{
                email:ProviderAttribute.GOOGLE_EMAIL,
                fullname:ProviderAttribute.GOOGLE_NAME,
                profilePicture:ProviderAttribute.GOOGLE_PICTURE
              },
              scopes:["profile","email" ,"openid"]
          });
         this.userPoolClient=new UserPoolClient(this,`${props.props.stackPrefix}-userpoolClient`,{
            userPool:props.cognitoUserPool.userPool,
            authFlows:{
              custom:true,
              userPassword:true,
              userSrp:true
            },
            oAuth:{
              callbackUrls:['http://localhost:4200/','https://dev.d23ob33zidll8x.amplifyapp.com','https://d3an0gbhwh9rx5.cloudfront.net'],
              logoutUrls:['http://localhost:4200/','https://dev.d23ob33zidll8x.amplifyapp.com','https://d3an0gbhwh9rx5.cloudfront.net'],
              flows:{
                authorizationCodeGrant:true,
                implicitCodeGrant:true
              },
              scopes:[OAuthScope.EMAIL,OAuthScope.COGNITO_ADMIN,OAuthScope.OPENID,OAuthScope.PROFILE,OAuthScope.PHONE]
            },
            supportedIdentityProviders:[UserPoolClientIdentityProvider.COGNITO,UserPoolClientIdentityProvider.FACEBOOK,UserPoolClientIdentityProvider.GOOGLE]
        });
        new UserPoolDomain(this,`${props.props.stackPrefix}-userpool-domain`,{
            userPool:props.cognitoUserPool.userPool,
            cognitoDomain:{
              domainPrefix:`${props.props.stackPrefix}`
            }
        });
        
    }
}