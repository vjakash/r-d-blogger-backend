
import { Construct } from "@aws-cdk/core";
import * as cognito from '@aws-cdk/aws-cognito';
import { ICognitoUserpoolProps } from "../../interface/cognito.interface";


export class CognitoUserPool extends Construct{
    readonly userPool:cognito.UserPool;

    constructor(scope: Construct, id: string, props: ICognitoUserpoolProps){
        super(scope,id);
        this.userPool= new cognito.UserPool(this,`${props.props.stackPrefix}-userpool`,{
            lambdaTriggers:{
              postConfirmation:props.triggers.postConfirmation
            },
            userPoolName:`${props.props.stackPrefix}-userpool`,
            selfSignUpEnabled:true,
            standardAttributes:{
              email:{mutable:true,required:true}
            },
            autoVerify:{
                email:true
            },
            accountRecovery:cognito.AccountRecovery.EMAIL_ONLY,
            passwordPolicy:{
                requireUppercase:false
            },
            userVerification:{
                emailBody:'{##Verify Email##}',
                emailStyle:cognito.VerificationEmailStyle.LINK,
                emailSubject:'Blogger app Verification email'
            }
        });
    }
}