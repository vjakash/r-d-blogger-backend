import { Construct } from "@aws-cdk/core";
import * as ec2 from '@aws-cdk/aws-ec2'
import InfraStackProps from "../../constants";


export class BloggerAppVPC extends Construct{
    readonly vpc:any;

    constructor(scope: Construct, id: string, props: typeof InfraStackProps){
        super(scope,id);
        this.vpc = ec2.Vpc.fromLookup(this,`${props.stackPrefix}-Vpc`,{isDefault: false,vpcId: "vpc-086cc1570410a33b1" });
    }
}