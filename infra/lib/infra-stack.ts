import * as cdk from '@aws-cdk/core';
import InfraStackProps from './constants';
import { BastionHost } from './resources/bastion/bastion';
import { CognitoUserPool } from './resources/cognito/cognito';
import { CognitoUserPoolClient } from './resources/cognito/userPoolClient';
import { Lambdas } from './resources/lambdas/lambdas';
import { PrismaLayer } from './resources/lambdas/prismaLayer';
import { Triggers } from './resources/lambdas/triggers';
import { BloggerAppVPC } from './resources/network/vpc';
import { RdsMysqlDB } from './resources/table/rds-mysql';

class InfraStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: typeof InfraStackProps) {
    super(scope, id, props);    
    const bloggerAppVPC=new BloggerAppVPC(this,`${props.stackPrefix}-vpc`,props);

    const rds=new RdsMysqlDB(this,`${props.stackPrefix}-rds-db`,{
      props,
      bloggerAppVPC
    });
    new BastionHost(this,`${props.stackPrefix}-ec2-bastionhost`,{
      props,
      bloggerAppVPC
    })
    const prismaLayer=new PrismaLayer(this,`${props.stackPrefix}-lambdaLayer`,{
      props
    });

    const triggers=new Triggers(this,`${props.stackPrefix}-triggers`,{
      bloggerAppVPC,
      props,
      rdsInstance:rds,
      prismaLayer:prismaLayer.prismaLayer
    })
    const userPool=new CognitoUserPool(this,`${props.stackPrefix}-cognito-userpool`,{
      props,
      triggers
    });

    const userPoolClient=new CognitoUserPoolClient(this,`${props.stackPrefix}-cognito-userpool-client`,{
      props,
      cognitoUserPool:userPool
    })

    userPoolClient.node.addDependency(userPool);

    
    
    new Lambdas(this,`${props.stackPrefix}-lambdas`,{
      bloggerAppVPC,
      cognitoUserPool:userPool,
      props,
      rdsInstance:rds,
      prismaLayer:prismaLayer.prismaLayer
    })
    
  }
}

export default InfraStack;
