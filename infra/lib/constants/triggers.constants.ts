import {  ITriggers } from "../interface/lambda.interface";

export const triggers:ITriggers={
    postConfirmation:{
        id:'postConfirmation',
        name:'postConfirmation',
        lambda:{
            code:'triggers',
            handler:'postConfirmation',
        }
    }
}