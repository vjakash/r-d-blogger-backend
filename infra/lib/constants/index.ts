const InfraStackProps = {
  stackName: 'blogger-app',
  stackPrefix: 'blogger-app',
  region: 'ap-south-1',
  stage: 'test',
  uiBucketName: 'blogger-app',
  env:{ 
    account: process.env.CDK_DEFAULT_ACCOUNT, 
    region: 'ap-south-1'
  }
};

export default InfraStackProps;
