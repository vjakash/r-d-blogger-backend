import { ILambdasConstant } from "../interface/lambda.interface";

export const lambdas:ILambdasConstant[]=[{
    resourceName:'api',
    childResource:[{
        resourceName:'test',
        methods:[{
            id:'healthCheck',
            name:'healthCheck',
            method: 'GET',
            auth: false,
            lambda:{
                code:'test',
                handler:'healthCheck',
            }
        }]
    },{
        resourceName:'posts',
        childResource:[{
            resourceName:'getAllPosts',
            methods:[{
                id:'getAllPosts',
                name:'getAllPosts',
                method: 'GET',
                auth: false,
                lambda:{
                    code:'posts',
                    handler:'getAllPosts',
                }
            }]
        },{
            resourceName:'getPostById',
            childResource:[{
               resourceName:'{id}',
               methods:[{
                    id:'getPostById',
                    name:'getPostById',
                    method: 'GET',
                    auth: false,
                    lambda:{
                        code:'posts',
                        handler:'getPostById',
                    }
                }]
            }]
        },{
            resourceName:'postComment',
            methods:[{
                id:'postComment',
                name:'postComment',
                method: 'POST',
                auth: true,
                lambda:{
                    code:'posts',
                    handler:'postComment',
                }
            }]
        },{
            resourceName:'likePost',
            methods:[{
                id:'likePost',
                name:'likePost',
                method: 'POST',
                auth: true,
                lambda:{
                    code:'posts',
                    handler:'likePost',
                }
            }]
        },{
            resourceName:'createPost',
            methods:[{
                id:'createPost',
                name:'createPost',
                method: 'POST',
                auth: true,
                lambda:{
                    code:'posts',
                    handler:'createPost',
                }
            }]
        }]
    },{
        resourceName:'users',
        childResource:[{
            resourceName:'followUser',
            methods:[{
                id:'followUser',
                name:'followUser',
                method: 'POST',
                auth: true,
                lambda:{
                    code:'posts',
                    handler:'followUser',
                }
            }]
        },{
            resourceName:'getUserById',
            childResource:[{
                resourceName:'{id}',
                methods:[{
                    id:'getUserById',
                    name:'getUserById',
                    method: 'GET',
                    auth: false,
                    lambda:{
                        code:'posts',
                        handler:'getUserById',
                    }
                }]
            }]
        }]
    }]
}];
