import { Construct } from "@aws-cdk/core";

export class EnvUtils {
  static get(scope: Construct, key: any) {
    return scope.node.tryGetContext(key);
  }
}
