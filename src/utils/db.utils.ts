/* eslint-disable prefer-destructuring */
import { PrismaClient } from '@prisma/client';
import logger  from './logging';

const globalAny: any = global;

logger.info('New connection Else');
if (!globalAny.prisma) {
  logger.info('New connection NOT');
  globalAny.prisma = new PrismaClient();
}

const  prisma:PrismaClient  = globalAny.prisma;

export default prisma;
