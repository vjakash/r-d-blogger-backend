import prisma from '../utils/db.utils';

export class Users {
    static async getUserById(event:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const user_email = event.pathParameters.id;
        
                const users = await prisma.users.findUnique({
                    where:{
                        email:user_email
                    },
                    select: {
                        // id: true,
                        name: true,
                        email: true,
                        image: true,
                        posts: {
                            select: {
                                id: true,
                                title: true,
                                post: true,
                                cover_image: true,
                                created_at: true,
                                likes_likesToposts: {
                                    select: {
                                        id: true,
                                        users: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                comments: {
                                    select: {
                                        id: true,
                                        comment: true,
                                        created_at: true,
                                        users: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                        followers_followers_followed_byTousers: {
                                            select: {
                                                users_followers_followingTousers: {
                                                    select: {
                                                        // id: true,
                                                        name: true,
                                                        email: true,
                                                        image: true,
                                                    }
                                                }
                                            }
                                        },
                                        followers_followers_followingTousers: {
                                            select: {
                                                users_followers_followed_byTousers: {
                                                    select: {
                                                        // id: true,
                                                        name: true,
                                                        email: true,
                                                        image: true,
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        followers_followers_followed_byTousers: {
                            select: {
                                users_followers_followingTousers: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        followers_followers_followingTousers: {
                            select: {
                                users_followers_followed_byTousers: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        }
                    }
                });
                resolve(users);
            } catch (err) {
                reject(err);
            }
        });
    }
}

