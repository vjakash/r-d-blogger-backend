import prisma from '../utils/db.utils';
import logger from '../utils/logging';


export class TriggerService{

    static createUser(event:any): Promise<any>{
        return new Promise(async (resolve, reject) => {
            try {
                const {name,email,picture}=event.request.userAttributes;
                logger.info("picture",picture)
                const user=await prisma.users.findUnique({
                    where:{
                        email
                    }
                });
                if(!user){
                    await prisma.users.create({
                        data:{
                            name,
                            email,
                            image:picture?.length>0?picture:`https://ipftp-resources.s3.amazonaws.com/public/toy-faces/ToyFaces_${this.getRandom()}.jpg`
                        }
                    })
                }
                resolve("");
            } catch (err) {
                reject(err);
            }
        });
    }

    static getRandom(){
        return Math.floor(Math.random() * 6)
    }
}