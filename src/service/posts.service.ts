import prisma from '../utils/db.utils';
import logger from '../utils/logging';

export class Posts {
    static async getAllPosts(_event: any): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                logger.info(_event);
                const posts = await prisma.posts.findMany({
                    orderBy: {
                        likes_likesToposts: {
                            _count: 'desc',
                        }
                    },
                    select: {
                        id: true,
                        title: true,
                        post: true,
                        cover_image: true,
                        created_at: true,
                        likes_likesToposts: {
                            select: {
                                id: true,
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        comments: {
                            select: {
                                id: true,
                                comment: true,
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        users: {
                            select: {
                                // id: true,
                                name: true,
                                email: true,
                                image: true,
                                followers_followers_followed_byTousers: {
                                    select: {
                                        users_followers_followingTousers: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                followers_followers_followingTousers: {
                                    select: {
                                        users_followers_followed_byTousers: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                resolve(posts);
            } catch (err) {
                reject(err);
            }
        });
    }

    static async getPostById(event:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const post_id = +event.pathParameters.id;
                const posts = await prisma.posts.findUnique({
                    where: {
                        id: post_id
                    },
                    select: {
                        id: true,
                        title: true,
                        post: true,
                        cover_image: true,
                        created_at: true,
                        likes_likesToposts: {
                            select: {
                                id: true,
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        comments: {
                            select: {
                                id: true,
                                comment: true,
                                created_at: true,
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        users: {
                            select: {
                                // id: true,
                                name: true,
                                email: true,
                                image: true,
                                followers_followers_followed_byTousers: {
                                    select: {
                                        users_followers_followingTousers: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                followers_followers_followingTousers: {
                                    select: {
                                        users_followers_followed_byTousers: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                resolve(posts);
            } catch (err) {
                reject(err);
            }
        });
    }

    static async likePost(event:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const { post_id, status, user_email } = JSON.parse(event.body);
                // const user = await prisma.users.findMany({
                //     where: {
                //         email: user_email
                //     }
                // });
                const like = await prisma.likes.findMany({
                    where: {
                        liked_by: user_email
                    }
                })
                if (like.length === 0 && status) {
                    await prisma.likes.create({
                        data: {
                            post_id,
                            liked_by: user_email
                        }
                    });
                } else if(like.length>0 && !status) {
                    await prisma.likes.deleteMany({
                        where: {
                            liked_by: user_email
                        }
                    });
                }
                const post = await prisma.posts.findUnique({
                    where: {
                        id: post_id
                    },
                    select: {
                        id: true,
                        title: true,
                        post: true,
                        cover_image: true,
                        created_at: true,
                        likes_likesToposts: {
                            select: {
                                id: true,
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        comments: {
                            select: {
                                id: true,
                                comment: true,
                                created_at: true,
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        users: {
                            select: {
                                // id: true,
                                name: true,
                                email: true,
                                image: true,
                                followers_followers_followed_byTousers: {
                                    select: {
                                        users_followers_followingTousers: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                followers_followers_followingTousers: {
                                    select: {
                                        users_followers_followed_byTousers: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                resolve(post);
            } catch (err) {
                reject(err);
            }
        });
    }

    static async postComment(event:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const { post_id, comment, user_email } = JSON.parse(event.body);
                // const user = await prisma.users.findMany({
                //     where: {
                //         email: user_email
                //     }
                // });
                 await prisma.comments.create({
                    data: {
                        comment,
                        post_id,
                        commented_by: user_email
                    }
                })
                const post = await prisma.posts.findUnique({
                    where: {
                        id: post_id
                    },
                    select: {
                        id: true,
                        title: true,
                        post: true,
                        cover_image: true,
                        created_at: true,
                        likes_likesToposts: {
                            select: {
                                id: true,
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        comments: {
                            select: {
                                id: true,
                                comment: true,
                                created_at: true,
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        users: {
                            select: {
                                // id: true,
                                name: true,
                                email: true,
                                image: true,
                                followers_followers_followed_byTousers: {
                                    select: {
                                        users_followers_followingTousers: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                followers_followers_followingTousers: {
                                    select: {
                                        users_followers_followed_byTousers: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                resolve(post);
            } catch (err) {
                reject(err);
            }
        });
    }

    static async followUser(event:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const { author_id, user_email } = JSON.parse(event.body);
                // const user = await prisma.users.findMany({
                //     where: {
                //         email: user_email
                //     }
                // });

                const follow = await prisma.followers.findMany({
                    where: {
                        followed_by: user_email,
                        following: author_id
                    }
                });

                if (follow.length === 0) {
                    await prisma.followers.create({
                        data: {
                            followed_by: user_email,
                            following: author_id
                        }
                    })
                } else {
                    await prisma.followers.deleteMany({
                        where: {
                            followed_by: user_email,
                            following: author_id
                        }
                    });
                }

                const updated_user = await prisma.users.findUnique({
                    where: {
                        email: author_id
                    },
                    select: {
                        // id: true,
                        name: true,
                        email: true,
                        image: true,
                        posts: {
                            select: {
                                id: true,
                                title: true,
                                post: true,
                                cover_image: true,
                                created_at: true,
                                likes_likesToposts: {
                                    select: {
                                        id: true,
                                        users: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                comments: {
                                    select: {
                                        id: true,
                                        comment: true,
                                        created_at: true,
                                        users: {
                                            select: {
                                                // id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                users: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                        followers_followers_followed_byTousers: {
                                            select: {
                                                users_followers_followingTousers: {
                                                    select: {
                                                        // id: true,
                                                        name: true,
                                                        email: true,
                                                        image: true,
                                                    }
                                                }
                                            }
                                        },
                                        followers_followers_followingTousers: {
                                            select: {
                                                users_followers_followed_byTousers: {
                                                    select: {
                                                        // id: true,
                                                        name: true,
                                                        email: true,
                                                        image: true,
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        followers_followers_followed_byTousers: {
                            select: {
                                users_followers_followingTousers: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        followers_followers_followingTousers: {
                            select: {
                                users_followers_followed_byTousers: {
                                    select: {
                                        // id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        }
                    }
                });
                resolve(updated_user);
            } catch (err) {
                reject(err);
            }
        });
    }

    static async createPost(event:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const { title,post,cover_image, user_email } = JSON.parse(event.body);
                // const user = await prisma.users.findMany({
                //     where: {
                //         email: user_email
                //     }
                // });
                await prisma.posts.create({
                    data:{
                        title,
                        posted_by:user_email,
                        post,
                        cover_image
                    }
                })
                resolve("Blog Published");
            } catch (err) {
                reject(err);
            }
        });
    }
}

