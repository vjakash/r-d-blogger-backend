import { TriggerService } from '../service/triggers.service';
import  logger  from '../utils/logging';
import { sendErrorResponse, sendSuccessResponse } from '../utils/response';

class TriggersApi {
  static async postConfirmation(_event: any,_context:any, callback:any): Promise<any> {
      try {
        logger.info('Enterd postConfirmation');       
        await TriggerService.createUser(_event);
        callback(null, _event);
        return sendSuccessResponse("Go serverless")
      } catch (err:any) {
        logger.error(err);
        callback(null, _event);
        return sendErrorResponse(err);
      }
  }
}

export const {postConfirmation} = TriggersApi;